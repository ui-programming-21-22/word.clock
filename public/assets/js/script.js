const options = {
	method: 'GET',
	headers: {
		'X-RapidAPI-Key': '28f297a0b1msh7609c059ab3048bp1675cajsn8a47c95a591e',
		'X-RapidAPI-Host': 'sun-calculator.p.rapidapi.com'
	}
};
	
fetch('https://sun-calculator.p.rapidapi.com/sunrise/?date=2022-10-24&lat=52.8365&lng=6.9341', options)
.then(response => response.json())
.then(response => console.log(response))
.catch(err => console.error(err));

var myDate = new Date( 1666592044 *1000);
console.log(myDate.toLocaleString());

var myDates = new Date( 1666628316 *1000);
console.log(myDates.toLocaleString());


var hours, minutes, to, past, off;
var progress;
var chars = 'abcdefghijklmnopqrstuvwxyz';

var changed = function () 
{
	var now = new Date();
	var hour = now.getHours();
	var minute = now.getMinutes();
	var offset;
	var next;
	hour = hour % 12;
	minute = minute - minute % 5;
	for (var i in minutes) 
    {
		minutes[i].removeClass('on');
	}
	for (var i in hours) 
    {
		hours[i].removeClass('on');
	}
	to.removeClass('on');
	past.removeClass('on');
	if (minute > 30) 
    {
		hours[(hour + 1) % 12].addClass('on');
		to.addClass('on');
	} else 
    {
		hours[hour].addClass('on');
		if (minute !== 0) 
        {
			past.addClass('on');
		}
	}
	offset = minute > 30 ? 60 - minute : minute;
	if (offset in minutes) 
    {
		minutes[offset].addClass('on');
	} 
    else if (offset === 25) 
    {
		minutes[20].addClass('on');
		minutes[5].addClass('on');
	}
	now.setTime(Date.now());
	next = new Date(now.getTime());
	next.setMinutes(minute + 5);
	next.setSeconds(0);
	next.setMilliseconds(0);
	setTimeout(changed, next - now);
};

var adjustProgress = function () 
{
	var now = new Date();
	var passed = now.getMinutes() % 5 * 60 + now.getSeconds();
	var percent = passed / (5 * 60) * 100;
	progress.width(percent + '%').css('transition', 'width 1s linear');
	if (percent < 0.5) 
    {
		progress.hide();
		setTimeout(function () 
        {
			progress.fadeIn();
		}, 500);
	}
	setTimeout(adjustProgress, 1000);
};

var first = function () 
{
	off.each(function () 
    {
		var c = chars.charAt(Math.floor(Math.random() * chars.length));
		$(this).text(c);
	});
	setTimeout(function () 
    {
		progress.fadeIn();
	}, 1000);
};

$(function () 
{
	minutes = 
    {
		0: $('#clock #m_0'),
		5: $('#clock #m_5'),
		10: $('#clock #m_10'),
		15: $('#clock #m_15'),
		20: $('#clock #m_20'),
		30: $('#clock #m_30')
	};

	hours = 
    {
		0: $('#clock #h_0'),
		1: $('#clock #h_1'),
		2: $('#clock #h_2'),
		3: $('#clock #h_3'),
		4: $('#clock #h_4'),
		5: $('#clock #h_5'),
		6: $('#clock #h_6'),
		7: $('#clock #h_7'),
		8: $('#clock #h_8'),
		9: $('#clock #h_9'),
		10: $('#clock #h_10'),
		11: $('#clock #h_11')
	};

	to = $('#clock #to');
	past = $('#clock #past');
	off = $('#clock .off');
	progress = $('#progressbar');
	changed();
	adjustProgress();
	first();

	
});

function myFunction() 
{
	var element = document.body;
	element.classList.toggle("dark-mode")
	let clock = document.getElementById("clock");
	clock.classList.toggle("light-mode");
	let words = document.getElementsByClassName("word");
	words = Array.from(words);
	words.forEach(function (element) {
		element.classList.toggle("light-words")
	});
};
